// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.
import 'package:flutter/material.dart';
import 'package:flutter_questionnaire/main.dart';
import 'package:flutter_test/flutter_test.dart';


void main() {
    testWidgets('Load the widget to the test', (WidgetTester tester) async {
        
        final assetManagementAns = find.text('Asset Management');
        final morethan10kAns = find.text('More than 10,000');
        final europeAns = find.text('Europe');
        final sixToNineAns = find.text('6-9 months');
        
        await tester.pumpWidget(MaterialApp(home : App()));
        expect(find.text('Asset Management'),findsOneWidget);
        await tester.tap(assetManagementAns);
        await tester.pump();

        expect(find.text('More than 10,000'),findsOneWidget);
        
        await tester.tap(morethan10kAns);
        await tester.pump();
        
        expect(find.text('Europe'),findsOneWidget);

        await tester.tap(europeAns);
        await tester.pump();
        
        expect(find.text('6-9 months'),findsOneWidget);

        await tester.tap(sixToNineAns);
        await tester.pump();

        expect(find.text('What is the nature of your business needs?'),findsOneWidget);
        expect(find.text('Asset Management'),findsOneWidget);
        expect(find.text('What is the expected size of the user base?'),findsOneWidget);
        expect(find.text('More than 10,000'),findsOneWidget);
        expect(find.text('In which region would the majority of the user base be?'),findsOneWidget);
        expect(find.text('Europe'),findsOneWidget);
        expect(find.text('What is the expected project duration?'),findsOneWidget);
        expect(find.text('6-9 months'),findsOneWidget);
    });
}